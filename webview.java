//隐藏webview的缩放按钮 适用于3.0和以后
	public void setZoomControlGoneX(WebSettings view ,Object[] args){
		 Class classType = view.getClass();
		 try { 
		     Class[] argsClass = new Class[args.length];  
		   
		     for (int i = 0, j = args.length; i < j; i++) {  
		         argsClass[i] = args[i].getClass();  
		     }
		     Method[] ms= classType.getMethods();
		     for (int i = 0; i < ms.length; i++) {
		    	 if(ms[i].getName().equals("setDisplayZoomControls")){
		    		 try {
		    			 ms[i].invoke(view, false);
						} catch (Exception e) {
							e.printStackTrace();
						}
		    		 break;
		    	 }
				//Log.e("test", ">>"+ms[i].getName());
			}

		}catch (Exception e) {
			e.printStackTrace();
		}
		 
	}
	
	
	//隐藏webview的缩放按钮 适用于3.0以前
	public void setZoomControlGone(View view) {  
	    Class classType;  
	    Field field;  
	    try {  
	        classType = WebView.class;  
	        field = classType.getDeclaredField("mZoomButtonsController");  
	        field.setAccessible(true);  
	        ZoomButtonsController mZoomButtonsController = new ZoomButtonsController(view);  
	        mZoomButtonsController.getZoomControls().setVisibility(View.GONE);  
	        try {  
	            field.set(view, mZoomButtonsController);  
	        } catch (IllegalArgumentException e) {  
	            e.printStackTrace();  
	        } catch (IllegalAccessException e) {  
	            e.printStackTrace();  
	        }  
	    } catch (SecurityException e) {  
	        e.printStackTrace();  
	    } catch (NoSuchFieldException e) {  
	        e.printStackTrace();  
	    }  
	}  

调用：
                weView.loadUrl("file:///android_asset/help.html");
		int sysVersion = Integer.parseInt(VERSION.SDK);
		if(sysVersion>=11){
			setZoomControlGoneX(weView.getSettings(),new Object[]{false});
		}else{
			setZoomControlGone(weView);
		}